﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Geolocation.Geo
{
    public class GeoResult
    {

        public Geometry Geometry { get; set; }
        public string formatted_address { get; set; }

    }
}