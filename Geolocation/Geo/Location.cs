﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Geolocation.Geo
{
    public class Location
    {
        public decimal lat { get; set; }
        public decimal lng { get; set; }
    }
}