﻿using Geolocation.NetGeo;
using Geolocation.Objetos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Geolocation
{
    public partial class Foxconn : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        //coletando dados do HTML para enviar a API de Geolocation

        protected void rota_Click(object sender, EventArgs e)
        {

            //Coletando os endereços informados pelo usuario
            Enderecos enderecos = new Enderecos();

            enderecos.address = pinicial.Text;
            enderecos.waypoint = waypoint.Text;
            enderecos.final_address = pfinal.Text;
            
            //Passando para API de Geolocation
            LocationGeo locationgeo = new LocationGeo();
            List<string> lenderecos = new List<string>();
            List<Glocation> locationarray = new List<Glocation>();

            lenderecos.Add(enderecos.address);
            lenderecos.Add(enderecos.waypoint);
            lenderecos.Add(enderecos.final_address);
                        

            locationarray = locationgeo.BuscaGeo(lenderecos);

            //Criando objeto para chamar a API de rotas
            RoutesGeo routesgeo = new RoutesGeo();
            IList<Legs> resultrotas = new List<Legs>();
            Distance distance = new Distance();
            Duration duration = new Duration();
            List<Distance> ldistance = new List<Distance>();
            List<Duration> lduration = new List<Duration>();
            //chamando a api para calcular a rota
            resultrotas = routesgeo.BuscaRotas(locationarray);
            distance = resultrotas[0].distance;
            duration = resultrotas[0].duration;

            ldistance.Add(distance);
            lduration.Add(duration);

            /*----------Passando as informaçoes tratadas para o Front-end--------------*/
            //populando grid com informaçoes de geolocation
            GridAddress.DataSource = locationarray;
            GridAddress.DataBind();
            GridDistancia.DataSource = ldistance;
            GridDistancia.DataBind();
            GridDuracao.DataSource = lduration;
            GridDuracao.DataBind();

            
           
        }

   















    }
}