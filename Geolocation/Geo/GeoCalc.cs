﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Net;
using System.Text;
using System.Data;
using Geolocation.Geo;
using System.Collections.Generic;

namespace Geolocation.Geo
{
    public class GeoCalc
    {


        public Dictionary<string, string> buscarend(string address)
        {
            Dictionary<string, string> diclatlong = new Dictionary<string, string>();
            string str = "http://maps.googleapis.com/maps/api/geocode/json?address=" + address + "&sensor=false";
            WebResponse response = null;
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(str);
                request.Method = "GET";
                response = request.GetResponse();
                if (response != null)
                {
                    string json_result = null;
                    using (Stream stream = response.GetResponseStream())
                    {
                        using (StreamReader streamReader = new StreamReader(stream))
                        {
                            json_result = streamReader.ReadToEnd();
                          
                        }
                    }
                    GeoResponse geoResponse = JsonConvert.DeserializeObject<GeoResponse>(json_result);
                  

                    if (geoResponse.Status == "OK")
                    {
                        int count = geoResponse.Results.Length;
                        for (int i = 0; i < count; i++)
                        {

                            diclatlong.Add("latitude", geoResponse.Results[i].Geometry.location.lat.ToString());
                            diclatlong.Add("longitude", geoResponse.Results[i].Geometry.location.lng.ToString());

                        }
                    }
                    else
                    {
                        diclatlong.Add("Lat", "-0");
                        diclatlong.Add("Lng", "-0");
                    }
                }
            }
            catch
            { throw new Exception("JSON response failed."); }
            finally
            {
                if (response != null)
                {
                    response.Close();
                    response = null;
                }
            }
            return diclatlong;
        }





    }
}