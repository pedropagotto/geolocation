﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net;
using System.Text;
using System.Data;
using Geolocation.Geo;
using System.Collections.Generic;
using Geolocation.Objetos;

namespace Geolocation.NetGeo
{
    public class RoutesGeo
    {


        public IList<Legs> BuscaRotas(List<Glocation> locationarray)
        {
            
            //list de retorno de consulta ao webservice de rotas
            List<string> results = new List<string>();
            IList<Legs> resultsearch = new List<Legs>();
            /*----------Montando a URL do webservice-----------*/

            //URL inicial com parametros de origem e destino
            string str_webservice = "https://maps.googleapis.com/maps/api/directions/json?origin={0},{1}&destination={2},{3}";
            str_webservice = string.Format(str_webservice,locationarray[0].lat.ToString(),locationarray[0].lon.ToString(),locationarray[2].lat.ToString(),locationarray[2].lon.ToString());
            //String com waypoints para unir junto a URL inicial e google key

            string str_waypoints = "&waypoints:optimize:true|" + locationarray[1].lat+","+locationarray[1].lon;

            //google key

            string gkey = "&key=AIzaSyCvQSyQ3fB2MAwYoOClZdLhgzFjkcNuKIU";

            //URL webservice completo
            string str = str_webservice + str_waypoints + gkey;

            //declarando o response
            WebResponse response = null;

            //iniciando as consultas e tratativas do webservice
            try
            {

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(str);
                request.Method = "GET";
                response = request.GetResponse();

                if (response != null)
                {
                    string json_result = null;
                    using (Stream stream = response.GetResponseStream())
                    {
                        using (StreamReader streamReader = new StreamReader(stream))
                        {
                            json_result = streamReader.ReadToEnd();

                        }
                    }

                    JObject search = JObject.Parse(json_result);

                    IList<JToken> sresult = search["routes"].Children()["legs"].Children().ToList();
                    

                    foreach (JToken result in sresult)
                    {
                        Legs legs  = JsonConvert.DeserializeObject<Legs>(result.ToString());
                        resultsearch.Add(legs);
                    }
                }
            
            }
            catch
            { throw new Exception("JSON response failed."); }
            finally
            {
                if (response != null)
                {
                    response.Close();
                    response = null;
                }
            }

            return resultsearch;


        }




    }
}