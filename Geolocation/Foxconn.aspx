﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Foxconn.aspx.cs" Inherits="Geolocation.Foxconn" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Demo : Calculo de Rotas</title>
</head>
<body>
    <h1>Demonstração de Calculo de Rotas :</h1>
    <h3>Modo de utilização , informe o nome do endereço utilize uma virgula e o nome da cidade</h3>
    <br />
    <br />
    

    <form id="consultgeo" runat="server">

    <div>
       <label id="ptoini">Ponto Inicial : </label> <asp:TextBox ID="pinicial" runat="server"></asp:TextBox><br /><br />
       <label id="ptoway">Ponto de Parada: </label> <asp:TextBox ID="waypoint" runat="server"></asp:TextBox><br /><br />
       <label id="ptofim">Ponto Final : </label><asp:TextBox ID="pfinal" runat="server"></asp:TextBox>
        <asp:Button ID="rota" runat="server" Text="Montar Rota" OnClick="rota_Click"/>
    
        <br />
        <br />
        <br />
        <br />
        <h2>Coleta do Endereço Completo informado pelo Usuario, Latitude e Longitude: </h2>
        <asp:GridView ID="GridAddress" runat="server" AutoGenerateColumns="true"></asp:GridView>
        <br />
        <h2>Coleta dos dados de Rotas: </h2>
        <h3>Distancia</h3>
        <asp:GridView ID="GridDistancia" runat="server" AutoGenerateColumns="true">
        </asp:GridView>
        <br />
        <h3>Duracao</h3>
        <asp:GridView ID="GridDuracao" runat="server" AutoGenerateColumns="true">
        </asp:GridView>

        

    </div>
    </form>
  
    

</body>

</html>
