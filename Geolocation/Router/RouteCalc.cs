﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net;
using System.Text;
using System.Data;
using Geolocation.Geo;
using System.Collections.Generic;

namespace Geolocation.Router
{
    public class RouteCalc
    {


        public List<string>  rotas (string rua){
            //list de retorno de consulta ao webservice de rotas
            List<string> results = new List<string>();

            //link do webservice de rotas
            string str = "https://maps.googleapis.com/maps/api/directions/json?origin=-22.7169797,-46.7766716&destination=-22.7168764,-46.7689096&key=AIzaSyCvQSyQ3fB2MAwYoOClZdLhgzFjkcNuKIU";
            //declarando o response
            WebResponse response = null;

            //iniciando as consultas e tratativas do webservice
            try
            {

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(str);
                request.Method = "GET";
                response = request.GetResponse();

                if (response!= null)
                {
                    string json_result = null;
                    using (Stream stream = response.GetResponseStream())
                    {
                        using (StreamReader streamReader = new StreamReader(stream))
                        {
                            json_result = streamReader.ReadToEnd();

                        }
                    }

                    JObject search = JObject.Parse(json_result);
                    
                    // IList<JToken> rst = search["routes"].Children().ToList();
                    IList<JToken> rst = search["routes"].Children()["legs"].Children()["distance"].ToList();
                    IList < distance > resultsearch = new List<distance>();

                    foreach (JToken result in rst)
                    {
                        distance ds = JsonConvert.DeserializeObject<distance>(result.ToString());
                        resultsearch.Add(ds);
                    }
                    
                }
            }
            catch
            { throw new Exception("JSON response failed."); }
            finally
            {
                if (response != null)
                {
                    response.Close();
                    response = null;
                }
            }



            return results;
    }


    }
}