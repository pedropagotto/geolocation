﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Geocoding;
using Geocoding.Google;
using Geolocation.Objetos;

namespace Geolocation.NetGeo
{
    public class LocationGeo
    {



        //Metodo para busca de Geolocation , retorna latitude e longitude de endereços

        
        public List<Glocation> BuscaGeo(List<string> lenderecos)
        {
            //delcarando array de retorno
            List<Glocation> locationarray = new List<Glocation>(); 
            //declarando objeto da API GoogleGeocoder
            IGeocoder igeo = new GoogleGeocoder();
            //indice para o For
            int i;
            //For para busca de Geolocation dos endereços informados pelo usuario

            for (i= 0; i< lenderecos.Count ; i++ ) {
                //realizando a consulta na API Geocoding
                IEnumerable<Address> address = igeo.Geocode(lenderecos[i]);
                //Criando um objeto Glocation para que possa receber os dados da API
                Glocation glocation = new Glocation();
                //coletando as informaçoes retornadas pela API
                glocation.formatted_address = address.First().FormattedAddress;
                //transformando , em . para que a informaçao de latitude e longitude possa ser utilizada na API de Rotas
                glocation.lat = address.First().Coordinates.Latitude.ToString().Replace(",",".");
                glocation.lon = address.First().Coordinates.Longitude.ToString().Replace(",", ".");
                
                                
                //adicionando objeto com a informaçao completa no array de retorno
                locationarray.Add(glocation);  
            }

            //retorno do array completo com as informaçoes
                return locationarray;
            
            }

            
        


        public void bendereco()
        {
            IGeocoder igeo = new GoogleGeocoder();
            IEnumerable<Address> address = igeo.Geocode("rua uruguai , amparo");

            string latitude, longitude, formatted_address;

            formatted_address = address.First().FormattedAddress;
            latitude = address.First().Coordinates.Latitude.ToString();
            longitude = address.First().Coordinates.Longitude.ToString();
        }


    }
}







