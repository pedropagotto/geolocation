﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Geolocation.Objetos
{
    //Classe para trabalhar com os dados de Geolocation 
    public class Glocation
    {
        //attributos para consumir api de geolocation
        public string formatted_address { get; set; }
        public string  lat { get; set; }
        public string  lon { get; set; }
        public string place_id { get; set; }
        public int waypoint{ get; set; }

    }

    //Classe para coletar input do usuario
    public class Enderecos
    {
        //atributos para coletar input do usuario
        public string address { get; set; }
        public string waypoint { get; set; }
        public string final_address { get; set; }
    }



    public class Distance
    {
        public string text { get; set; }
        public string  value { get; set; }
    }


    public class Duration
    {
        public string text { get; set; }
        public string value { get; set; }
    }

    public class Legs
    {
        public Distance distance { get; set; }
        public Duration duration { get; set; }
    }

    public class Routes
    {
        public Legs legs { get; set; }
        public string Status { get; set; }
    }


}