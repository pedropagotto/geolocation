﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="Geolocation.WebForm2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
      <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 10pt;
        }
    </style>
</head>
<body>
    <form id="form2" runat="server">
    <asp:TextBox ID="txtLocation2" runat="server" Text=""></asp:TextBox>
    <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click1"/>
    <br />
    <br />


    
     <asp:GridView ID="GridView1" HeaderStyle-BackColor="#3AC0F2" HeaderStyle-ForeColor="White"
        runat="server" AutoGenerateColumns="false">
        
         <Columns>
            <asp:BoundField DataField="Key"/>
            <asp:BoundField DataField="Value"  />
          </Columns>
    </asp:GridView>
    </form>
</body>
</html>
